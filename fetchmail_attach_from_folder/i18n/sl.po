# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * fetchmail_attach_from_folder
#
# Translators:
# Matjaž Mozetič <m.mozetic@matmoz.si>, 2015
msgid ""
msgstr ""
"Project-Id-Version: server-tools (8.0)\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-09-29 11:14+0000\n"
"PO-Revision-Date: 2015-09-26 07:28+0000\n"
"Last-Translator: Matjaž Mozetič <m.mozetic@matmoz.si>\n"
"Language-Team: Slovenian (http://www.transifex.com/oca/OCA-server-tools-8-0/"
"language/sl/)\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n"
"%100==4 ? 2 : 3);\n"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_active
msgid "Active"
msgstr "Aktivno"

#. module: fetchmail_attach_from_folder
#: code:addons/fetchmail_attach_from_folder/wizard/attach_mail_manually.py:35
#, fuzzy, python-format
msgid "Attach emails manually"
msgstr "Ročno pripenjanje e-pošte"

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_attach_mail_manually
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "Attach mail manually"
msgstr "Ročno pripenjanje e-pošte"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folders_available
msgid "Available folders"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_body
msgid "Body"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_attach_mail_manually
msgid "Cancel"
msgstr "Preklic"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folders_only
msgid ""
"Check this field to leave imap inbox alone and only retrieve mail from "
"configured folders."
msgstr ""

#. module: fetchmail_attach_from_folder
#: code:addons/fetchmail_attach_from_folder/models/fetchmail_server.py:34
#, python-format
msgid "Confirm connection first."
msgstr ""

#. module: fetchmail_attach_from_folder
#: selection:fetchmail.server.folder,state:0
msgid "Confirmed"
msgstr ""

#. module: fetchmail_attach_from_folder
#: code:addons/fetchmail_attach_from_folder/models/fetchmail_server_folder.py:160
#, python-format
msgid "Could not fetch %s in %s on %s"
msgstr ""

#. module: fetchmail_attach_from_folder
#: code:addons/fetchmail_attach_from_folder/models/fetchmail_server_folder.py:140
#, python-format
msgid "Could not open mailbox %s on %s"
msgstr ""

#. module: fetchmail_attach_from_folder
#: code:addons/fetchmail_attach_from_folder/models/fetchmail_server_folder.py:145
#, python-format
msgid "Could not search mailbox %s on %s"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_create_uid
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_create_uid
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_create_uid
msgid "Created by"
msgstr "Ustvaril"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_create_date
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_create_date
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_create_date
msgid "Created on"
msgstr "Ustvarjeno"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_date
msgid "Date"
msgstr "Datum"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_delete_matching
msgid "Delete matched emails from server"
msgstr "Izbris ujemajočih se e-poštnih sporočil iz strežnika"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_delete_matching
msgid "Delete matches"
msgstr "Izbris ujemajočih"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_display_name
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_display_name
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_display_name
msgid "Display Name"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_domain
msgid "Domain"
msgstr "Domena"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_ids
msgid "Emails"
msgstr "E-poštna sporočila"

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "Fetch folder now"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_mail_field
msgid "Field (email)"
msgstr "Polje (e-pošta)"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_model_field
msgid "Field (model)"
msgstr "Polje (model)"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_model_order
msgid ""
"Field(s) to order by, this mostly useful in conjunction with 'Use 1st match'"
msgstr ""
"Polje(a) za razvrščanje. Uporabljajo se večinoma v sklopu 'Uporabi 1. "
"ujemanje'"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_domain
msgid "Fill in a search filter to narrow down objects to match"
msgstr "Izpolni iskalni filter za zožitev ujemajočih se objektov"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_flag_nonmatching
msgid "Flag emails in the server that don't match any object in Odoo"
msgstr ""
"Označi e-poštna sporočila na strežniku, ki se ne ujemajo z nobenim objektom "
"v Odoo"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_flag_nonmatching
msgid "Flag nonmatching"
msgstr "Označi ne ujemajoča"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_folder_id
msgid "Folder"
msgstr "Mapa"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_ids
msgid "Folders"
msgstr "Mape"

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "Folders available on server"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "Folders to monitor"
msgstr "Mape za nadziranje"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_email_from
msgid "From"
msgstr ""

#. module: fetchmail_attach_from_folder
#: code:addons/fetchmail_attach_from_folder/models/fetchmail_server_folder.py:202
#, python-format
msgid "General failure when trying to connect to %s server %s."
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_id
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_id
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_id
msgid "ID"
msgstr "ID"

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "INBOX.subfolder1"
msgstr "INBOX.podmapa1"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_match_first
msgid ""
"If there are multiple matches, use the first one. If not checked, multiple "
"matches count as no match at all"
msgstr ""
"Ko je več ujemajočih se sporočil, uporabi prvo. Če ni označeno, se več "
"ujemajočih se sporočil smatra kot, da sploh ni ujemajočih se sporočil"

#. module: fetchmail_attach_from_folder
#: code:addons/fetchmail_attach_from_folder/models/fetchmail_server_folder.py:111
#, python-format
msgid "Invalid folder %s!"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually___last_update
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail___last_update
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder___last_update
#, fuzzy
msgid "Last Modified on"
msgstr "Zadnjič posodobljeno"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_write_uid
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_write_uid
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_write_uid
msgid "Last Updated by"
msgstr "Zadnjič posodobil"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_write_date
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_write_date
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_write_date
msgid "Last Updated on"
msgstr "Zadnjič posodobljeno"

#. module: fetchmail_attach_from_folder
#: code:addons/fetchmail_attach_from_folder/models/fetchmail_server_folder.py:263
#, python-format
msgid "Mail attachment"
msgstr "E-poštna priponka"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_match_algorithm
msgid "Match algorithm"
msgstr "Algoritem ujemanja"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_msgid
msgid "Message id"
msgstr "ID sporočila"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_msg_state
msgid "Message state"
msgstr "Stanje sporočila"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_model_id
msgid "Model"
msgstr "Model"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_name
msgid "Name"
msgstr ""

#. module: fetchmail_attach_from_folder
#: selection:fetchmail.server.folder,state:0
msgid "Not Confirmed"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_object_id
msgid "Object"
msgstr "Objekt"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folders_only
msgid "Only folders, not inbox"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_model_order
msgid "Order (model)"
msgstr "Vrstni red (model)"

#. module: fetchmail_attach_from_folder
#: model:ir.model,name:fetchmail_attach_from_folder.model_fetchmail_server
msgid "POP/IMAP Server"
msgstr "POP/IMAP strežnik"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_path
msgid "Path"
msgstr "Pot"

#. module: fetchmail_attach_from_folder
#: selection:fetchmail.server.folder,msg_state:0
msgid "Received"
msgstr "Prejeto"

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "Reset Confirmation"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_attach_mail_manually
msgid "Save"
msgstr "Shrani"

#. module: fetchmail_attach_from_folder
#: selection:fetchmail.server.folder,msg_state:0
msgid "Sent"
msgstr "Poslano"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_sequence
msgid "Sequence"
msgstr "Zaporedje"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_server_id
msgid "Server"
msgstr "Strežnik"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_state
msgid "Status"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_subject
msgid "Subject"
msgstr "Zadeva"

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "Test & Confirm"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_match_algorithm
msgid "The algorithm used to determine which object an email matches."
msgstr ""
"Algoritem, ki se uporablja za določanje objekta, ki mu e-pošta priprada."

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_mail_field
msgid ""
"The field in the email used for matching. Typically this is 'to' or 'from'"
msgstr ""
"Polje v e-poštnem sporočilu, ki se uporablja za primerjavo. Tipično je to "
"'za' ali 'od'"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_model_field
msgid ""
"The field in your model that contains the field to match against.\n"
"Examples:\n"
"'email' if your model is res.partner, or 'partner_id.email' if you're "
"matching sale orders"
msgstr ""
"Polje modela, ki vsebuje polje za ujemanje.\n"
"Primeri:\n"
"'e-pošta' če je model res.partner ali 'partner_id.email', če ujemate "
"prodajne naloge"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_model_id
msgid "The model to attach emails to"
msgstr "Model, ki mu pripenjamo e-pošto"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_path
msgid ""
"The path to your mail folder. Typically would be something like 'INBOX."
"myfolder'"
msgstr "Pot do e-poštne mape. Običajno je to nekaj kot 'INBOX.mojamapa'"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,help:fetchmail_attach_from_folder.field_fetchmail_server_folder_msg_state
msgid "The state messages fetched from this folder should be assigned in Odoo"
msgstr "Stanje, ki se dodeli sporočilom prenesenim iz te mape."

#. module: fetchmail_attach_from_folder
#: code:addons/fetchmail_attach_from_folder/models/fetchmail_server.py:39
#, python-format
msgid "Unable to retrieve folders."
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_server_folder_match_first
msgid "Use 1st match"
msgstr "Uporabi 1. ujemanje"

#. module: fetchmail_attach_from_folder
#: model:ir.model.fields,field_description:fetchmail_attach_from_folder.field_fetchmail_attach_mail_manually_mail_wizard_id
msgid "Wizard id"
msgstr "ID čarovnika"

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "[('state', '=', 'open')]"
msgstr "[('state', '=', 'open')]"

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "email"
msgstr "e-pošta"

#. module: fetchmail_attach_from_folder
#: model:ir.model,name:fetchmail_attach_from_folder.model_fetchmail_attach_mail_manually
#, fuzzy
msgid "fetchmail.attach.mail.manually"
msgstr "Ročno pripenjanje e-pošte"

#. module: fetchmail_attach_from_folder
#: model:ir.model,name:fetchmail_attach_from_folder.model_fetchmail_attach_mail_manually_mail
#, fuzzy
msgid "fetchmail.attach.mail.manually.mail"
msgstr "Ročno pripenjanje e-pošte"

#. module: fetchmail_attach_from_folder
#: model:ir.model,name:fetchmail_attach_from_folder.model_fetchmail_server_folder
msgid "fetchmail.server.folder"
msgstr ""

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "name asc,type desc"
msgstr "name asc,type desc"

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_attach_mail_manually
msgid "or"
msgstr "ali"

#. module: fetchmail_attach_from_folder
#: model:ir.ui.view,arch_db:fetchmail_attach_from_folder.view_email_server_form
msgid "to,from"
msgstr "to,from"

#~ msgid "Mailbox %s not found!"
#~ msgstr "Poštni predal %s ni najden!"

#~ msgid "{'required': [('type', '!=', 'imap')]}"
#~ msgstr "{'required': [('type', '!=', 'imap')]}"
